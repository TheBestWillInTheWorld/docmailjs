//requires targetversion.js
var mailingGUID;

//docmail API variables
var accountType = 'Topup';

//track the status (set by async callbacks/PostMessage)
var orderStage = '';

$(document).ready(function () {	
	//handle enter press on inputs
	$(':input').keyup(function (e) {if (e.which == 13) getBalance();});
	
	$('#btnGetBalance').click(getBalance);
	$('#btnBuild').click(buildPostcard);
	$('#btnSubmit').click(processMailing);
	
	if ($('#txtImageURL').length===1){	
		console.log($('#txtImageURL').length);
		showImage($('#txtImageURL').val());
		$('#txtImageURL').keyup(function(){showImage($('#txtImageURL').val());});
	};
	
	if ($('#imgInput').length===1) $('#imgInput').on('change',function(){readURL(this);});
		
	$('#domain').append(location.protocol+'//'+document.domain);
	
	$('#receiver').attr('src', iFrameTarget);
});


window.addEventListener('message', function(e) {
		$('#result').empty();
		if (e.data.MailingGUID!=undefined)	mailingGUID=e.data.MailingGUID;
		$('#result').append('got callback: ' + JSON.stringify(e.data) );
		orderStage = e.data.method;
		
		//this effectively daisy-chains the asynchronous calls together in order
		if 		(orderStage === "CreateMailing") 		addDesignerTemplate()
		else if	(orderStage === "AddDesignerTemplate")	addDesignerImage()
		else if	(orderStage === "AddDesignerImage")		addDesignerText()
		else if	(orderStage === "AddDesignerText")		addAddress()		
});

var buildPostcard = function(){	
	createMailing();
}
