//requires:
//targetversion.js
//container.js
var mailingGUID;

//docmail API variables
var accountType = 'Topup';

$(document).ready(function () {	
	//handle enter press on inputs
	$(':input').keyup(function (e) {if (e.which == 13) getBalance();});
	
	$('#btnGetBalance').click(getBalance);
	$('#btnCreateMailing').click(createMailing);
	$('#btnAddDesignerTemplate').click(addDesignerTemplate);
	$('#btnAddDesignerImage').click(addDesignerImage);
	$('#btnAddDesignerText').click(addDesignerText);
	$('#btnAddAddress').click(addAddress);
	
	$('#btnSubmit').click(processMailing);
	
	if ($('#txtImageURL').length===1){	
		console.log('expecting image by URL');
		showImage($('#txtImageURL').val());
		$('#txtImageURL').keyup(function(){showImage($('#txtImageURL').val());});
	};
	
	if ($('#imgInput').length===1) $('#imgInput').on('change',function(){readURL(this);});
		
	$('#domain').append(location.protocol+'//'+document.domain);
	
	$('#receiver').attr('src', iFrameTarget);
});


window.addEventListener('message', function(e) {
		$('#result').empty();
		if (e.data.MailingGUID!=undefined)	mailingGUID=e.data.MailingGUID;
		$('#result').append('got callback: ' + JSON.stringify(e.data) );
});
