//requires targetversion.js
var mailingGUID;

//docmail API variables
var accountType = 'Topup';

$(document).ready(function () {	
	$('#domain').append(location.protocol+'//'+document.domain);
	$('#receiver').attr('src', iFrameTarget);
});

//incoming PostMessage should be handled by the individual page
// window.addEventListener('message', function(e) {
                    // $('#result').empty();
					// if (e.data.MailingGUID!=undefined)	mailingGUID=e.data.MailingGUID;
					// $('#result').append('got callback: ' + JSON.stringify(e.data) );
// });


var getBalance = function (){	
					$('#result').empty();
					var receiver = $('#receiver')[0].contentWindow;
					var data = {	'method'	  : 'getBalance',
									'user'		  : $('#txtUsr').val(),
									'password'	  : $('#txtPwd').val(),
									'accountType' : accountType
								};			
					console.log('sending');
					receiver.postMessage(data,targetDomain);
				};

var createMailing = function (){	
					$('#result').empty();
					var receiver = $('#receiver')[0].contentWindow;
					var data = {	'method'	  : 'createMailing',
									'user'		  : $('#txtUsr').val(),
									'password'	  : $('#txtPwd').val()
								};			
					console.log('sending');
					receiver.postMessage(data,targetDomain);
				};

var addDesignerTemplate = function (){
					$('#result').empty();	
					var receiver = $('#receiver')[0].contentWindow;
					var data = {	'method'	  : 'addDesignerTemplate',
									'user'		  : $('#txtUsr').val(),
									'password'	  : $('#txtPwd').val(),
									'mailingGUID' : mailingGUID
								};			
					console.log('sending');
					receiver.postMessage(data,targetDomain);
				};

var addDesignerImage = function (){	
					$('#result').empty();
					console.log('addDesignerImage');
					var receiver = $('#receiver')[0].contentWindow;			
					var data;
					var imageUrl;

					//have to define callback in case we're loading an image (async)
					var makeTheCall = function (base64Image){
						console.log('makeTheCall');
						data = {	'method'	      : 'addDesignerImage',
									'user'		      : $('#txtUsr').val(),
									'password'	      : $('#txtPwd').val(),
									'mailingGUID'     : mailingGUID,
									'partDisplayName' : 'Background photo',
									'imageName'		  : imageUrl,
									'imageUrl'		  : imageUrl,
									'imageData'		  : base64Image,
									'imageRotation'	  : '0',
									'imageFitOption'  : 'Crop'
								};			
						console.log('sending');
						receiver.postMessage(data,targetDomain);
					};
					
					if ($('#imgInput').length==1) {						
						console.log('getBase64Image');
						imageUrl = $('#imgInput').val();
						getBase64Image($('#imgPreview')[0],makeTheCall);
					}
					else {
						console.log('direct makeTheCall');
						imageUrl = $('#txtImageURL').val()
						makeTheCall('undefined');
					};
				};

var addDesignerText = function (){	
					$('#result').empty();
					var receiver = $('#receiver')[0].contentWindow;
					var data = {	'method'	  		: 'addDesignerText',
									'user'		  		: $('#txtUsr').val(),
									'password'	  		: $('#txtPwd').val(),
									'mailingGUID' 		: mailingGUID,
									'partDisplayName'	: 'Message',
									'textContent'		: $('#txtMessage').val(),
									'fontSize'			: 20,
									'fontName'			: 'Verdana',
									'bold'				: false,
									'italic'			: false,
									'underline'			: false,
									'textJustification' : 'Center',
									'fontColourRed'		: 255,
									'fontColourGreen'	: 255,
									'fontColourBlue'	: 255
								};			
					console.log('sending');
					receiver.postMessage(data,targetDomain);
				};

var addAddress = function (){	
					$('#result').empty();
					var receiver = $('#receiver')[0].contentWindow;
					var data = {	'method'	  : 'addAddress',
									'user'		  : $('#txtUsr').val(),
									'password'	  : $('#txtPwd').val(),
									'mailingGUID' : mailingGUID,
									'fullName'	  : $('#txtName').val(),
									'addressBlock': $('#txtAddress').val()
								};			
					console.log('sending');
					receiver.postMessage(data,targetDomain);
				};

var processMailing = function (){	
					$('#result').empty();
					var receiver = $('#receiver')[0].contentWindow;
					var data = {	'method'	  : 'processMailing',
									'user'		  : $('#txtUsr').val(),
									'password'	  : $('#txtPwd').val(),
									'mailingGUID' : mailingGUID
								};			
					console.log('sending');
					receiver.postMessage(data,targetDomain);
				};