//requires
// jquery.min.js
// xml2json.js
// jquery.soap.js

var wsUrl		= 'https://www.cfhdocmail.com/TestAPI2/DMWS.asmx';
var wsNamespace = 'https://www.cfhdocmail.com/LiveAutoAPI/';

function getFilename(url){
   if (url)   {
      var filename = url.replace(/^.*[\\\/]/, '').split("?")[0];
	  console.log('parsed filename: '+filename);
	  return filename;
   }
   return "";
}

//class-like function
function DocmailJS(){
		//instance variables protected within "Class" function
		var methodName='';
		var callbackMethod=undefined;
		
		//format the response as a JSON object and pass it back via callbackMethod
		var responseFormatter = function(soapResponse){
			console.log('responder');
			console.log(soapResponse);
			var xml 		= soapResponse.toXML();	
			var repsonse 	= $(xml).find(methodName+"Result").text();	
			console.log(xml);
			console.log(repsonse);
			
			var resultObj = $.parseJSON(repsonse);
			console.log(resultObj);
			
			var jsonString = '';
			$.each( resultObj, function( index, contents ) {
				console.log(contents);
				$.each( contents, function( key, value ) {
					if (jsonString !='' && key==='Key'){
						//this is a new key, & it's not the first; needs a comma
						jsonString=jsonString.concat(",");
					}
					//if it's a value, we need a separator
					if (key=='Value') jsonString=jsonString.concat(":");
					//write the string
					jsonString=jsonString.concat('"'+ value +'"');
				});
			});
			jsonString='{'+jsonString+'}';
			console.log('jsonString');
			console.log(jsonString);
			
			var response = $.parseJSON(jsonString);		
			response['method'] = methodName;
			callbackMethod(response);
			methodName = '';
		};

		//public wrappers for SOAP methods

		this.getBalance = function(username,password,accountType,returnFormat,callback){
			 methodName = 'GetBalance';
			 callbackMethod = callback;
			$.soap({
				url			: wsUrl,
				method		: methodName,
				SOAPAction	: '"'+ wsNamespace + methodName +'"', 
				data: {
						Username	: username,
						Password	: password,
						AccountType	: accountType,
						ReturnFormat: returnFormat
				},		
				success	: function (soapResponse) { responseFormatter(soapResponse)},
				error	: function (soapResponse) { responseFormatter(soapResponse)},		
				appendMethodToURL: false,        
				namespaceURL:	wsNamespace
			});
		};
		
	
		this.createMailing = function(
									  username,password,customerApplication,
									  productType,mailingName,mailingDescription,
									  isMono,isDuplex,deliveryType,courierDeliveryToSelf,
									  despatchASAP,despatchDate,addressNamePrefix,
									  addressNameFormat,discountCode,minEnvelopeSize,returnFormat,
									  callback
									  ){
			var params  = {
						Username			: username,
						Password			: password,
						CustomerApplication : customerApplication ,
						ProductType			: productType		,
						MailingName			: mailingName		,
						MailingDescription	: mailingDescription,
						IsMono				: isMono			,
						IsDuplex			: isDuplex			,
						DeliveryType		: deliveryType		,
						CourierDeliveryToSelf: courierDeliveryToSelf,
						DespatchASAP		: despatchASAP		,
						//can't pass a blank string or undefined - not sure how to pass "nothing" without excluding from the params object
						//DespatchDate		: despatchDate		,
						AddressNamePrefix	: addressNamePrefix	,
						AddressNameFormat	: addressNameFormat	,
						DiscountCode		: discountCode		,
						MinEnvelopeSize		: minEnvelopeSize	,
						ReturnFormat		: returnFormat	
				};
				
			 methodName = 'CreateMailing';
			 callbackMethod = callback;
			$.soap({
				url			: wsUrl,
				method		: methodName,
				SOAPAction	: '"'+ wsNamespace + methodName +'"', 
				data		: params,		
				success		: function (soapResponse) { responseFormatter(soapResponse)},
				error		: function (soapResponse) { responseFormatter(soapResponse)},		
				appendMethodToURL: false,        
				namespaceURL:	wsNamespace
			});
		};
	
		//AddDesignerTemplate(sUsr, sPwd, gMailingGUID, sTemplateLayout, sDocType, "", bBleedSupplied, 0, True, sReturnMessageFormat)
		this.addDesignerTemplate = function(
											username,password,mailingGUID,templateLayout,documentType,addressFontCode,bleedSupplied,
											copies,skipPreviewImageGeneration,returnFormat,
											callback
									  ){
			var params  = {
							Username		:username,
							Password		:password,
							MailingGUID		:mailingGUID,
							TemplateLayout	:templateLayout,
							DocumentType	:documentType,
							AddressFontCode	:addressFontCode,
							BleedSupplied	:bleedSupplied,
							Copies			:copies,
							SkipPreviewImageGeneration:skipPreviewImageGeneration,
							ReturnFormat	:returnFormat
				};
				
			 methodName = 'AddDesignerTemplate';
			 callbackMethod = callback;
			$.soap({
				url			: wsUrl,
				method		: methodName,
				SOAPAction	: '"'+ wsNamespace + methodName +'"', 
				data		: params,		
				success		: function (soapResponse) { responseFormatter(soapResponse)},
				error		: function (soapResponse) { responseFormatter(soapResponse)},		
				appendMethodToURL: false,        
				namespaceURL:	wsNamespace
			});
		};
	
		//AddDesignerImage(sUsr, sPwd, gMailingGUID, "Background photo", sImgFileName, OpenFileAsByteArray(sImgFilePath), dImgRotation, "Crop", sReturnMessageFormat)
		this.addDesignerImage = function(
											username,
											password,
											mailingGUID,
											partDisplayName,
											fileName,
											fileData,
											imageRotation,
											imageFitOption,
											returnFormat,
											callback
									  ){
			var params  = {
						  Username			: username,
						  Password        	: password,
						  MailingGUID    	: mailingGUID,
						  PartDisplayName  	: partDisplayName,
						  FileName         	: getFilename(fileName), //ensure filename is sanitised (some lazy code like me might just pass in the whole URL!
						  FileData         	: fileData,
						  ImageRotation    	: imageRotation,
						  ImageFitOption   	: imageFitOption,
						  ReturnFormat     	: returnFormat
				};                          
			console.log(params);
			 methodName = 'AddDesignerImage';
			 callbackMethod = callback;
			$.soap({
				url			: wsUrl,
				method		: methodName,
				SOAPAction	: '"'+ wsNamespace + methodName +'"', 
				data		: params,		
				success		: function (soapResponse) { responseFormatter(soapResponse)},
				error		: function (soapResponse) { responseFormatter(soapResponse)},		
				appendMethodToURL: false,        
				namespaceURL:	wsNamespace
			});
		};
	
		//AddDesignerText(sUsr, sPwd, gMailingGUID, "Message", sTextMessage, 0, "", False, False, False, "", 0, 0, 0, sReturnMessageFormat)
		this.addDesignerText = function(
											username,
											password,
											mailingGUID,
											partDisplayName,
											textContent,
											fontSize,
											fontName,
											bold,
											italic,
											underline,
											textJustification,
											fontColourRed,
											fontColourGreen,
											fontColourBlue,
											returnFormat,
											callback
									  ){
			var params  = {
						  Username			: username,
						  Password        	: password,
						  MailingGUID    	: mailingGUID,
						  PartDisplayName	: partDisplayName,
						  TextContent       : textContent,
						  FontSize          : fontSize,
						  FontName          : fontName,
						  Bold              : bold,
						  Italic            : italic,
						  Underline         : underline,
						  TextJustification : textJustification,
						  FontColourRed     : fontColourRed,
						  FontColourGreen   : fontColourGreen,
						  FontColourBlue	: fontColourBlue,				  
						  ReturnFormat     	: returnFormat
				};                          
			console.log(params);
			 methodName = 'AddDesignerText';
			 callbackMethod = callback;
			$.soap({
				url			: wsUrl,
				method		: methodName,
				SOAPAction	: '"'+ wsNamespace + methodName +'"', 
				data		: params,		
				success		: function (soapResponse) { responseFormatter(soapResponse)},
				error		: function (soapResponse) { responseFormatter(soapResponse)},		
				appendMethodToURL: false,        
				namespaceURL:	wsNamespace
			});
		};
		//AddAddress(sUsr, sPwd, gMailingGUID, "Красноармейская ул., 278, Кв. 54", "Ростов-на-Дону, Ростовская область", "344022", "Russian Federation", "", "", True, "", "", "", "Тарасова Татьяна Васильевна", "", "", "", "", "", "", "", "", "", "", "", -1, -1, -1, "", "", "", "", "", "", "", "", "", "", sReturnMessageFormat)
		this.addAddress = function(
											username,
											password,
											mailingGUID,
											address1,
											address2,
											address3,
											address4,
											address5,
											address6,
											useForProof,
											title,
											firstName,
											surname,
											fullname,
											jobTitle,
											companyName,
											email,
											telephone,
											directLine,
											mobile,
											facsimile,
											extraInfo,
											notes,
											customerAddressID,
											customerImportID,
											streamPages1,
											streamPages2,
											streamPages3,
											custom1,
											custom2,
											custom3,
											custom4,
											custom5,
											custom6,
											custom7,
											custom8,
											custom9,
											custom10,
											returnFormat,
											callback
									  ){
			var params  = {
						  Username			 : username,
						  Password        	 : password,
						  MailingGUID    	 : mailingGUID,
						  Address1			 : address1,
                          Address2           : address2,
                          Address3           : address3,
                          Address4           : address4,
                          Address5           : address5,
                          Address6           : address6,
                          UseForProof        : useForProof,
                          Title              : title,
                          FirstName          : firstName,
                          Surname            : surname,
                          Fullname           : fullname,
                          JobTitle           : jobTitle,
                          CompanyName        : companyName,
                          Email              : email,
                          Telephone          : telephone,
                          DirectLine         : directLine,
                          Mobile             : mobile,
                          Facsimile          : facsimile,
						  ExtraInfo          : extraInfo,
						  Notes              : notes,
						  CustomerAddressID  : customerAddressID,
						  CustomerImportID   : customerImportID,
						  //passing these as blank is bad as .net blows up on blank numerics...
                          //StreamPages1       : streamPages1,
                          //StreamPages2       : streamPages2,
                          //StreamPages3       : streamPages3,
                          Custom1            : custom1,
                          Custom2            : custom2,
                          Custom3            : custom3,
                          Custom4            : custom4,
                          Custom5            : custom5,
                          Custom6            : custom6,
                          Custom7            : custom7,
                          Custom8            : custom8,
                          Custom9            : custom9,
                          Custom10           : custom10,
						  ReturnFormat     	 : returnFormat
				};                          
			console.log(params);
			 methodName = 'AddAddress';
			 callbackMethod = callback;
			$.soap({
				url			: wsUrl,
				method		: methodName,
				SOAPAction	: '"'+ wsNamespace + methodName +'"', 
				data		: params,		
				success		: function (soapResponse) { responseFormatter(soapResponse)},
				error		: function (soapResponse) { responseFormatter(soapResponse)},		
				appendMethodToURL: false,        
				namespaceURL:	wsNamespace
			});
		};
		
		//ProcessMailing(sUsr, sPwd, gMailingGUID, sAppName, True, False, 0, "", "Topup", True, sEmailOnError, sEmailOnSuccess, "", "", sReturnMessageFormat)
		this.processMailing = function(
											username,
											password,
											mailingGUID,
											customerApplication, 
											submit,
											partialProcess,
											maxPriceExVAT, 
											pOReference,
											paymentMethod,
											skipPreviewImageGeneration, 
											emailSuccessList,
											emailErrorList, 
											httpPostOnSuccess,
											httpPostOnError, 
											returnFormat,
											callback
									  ){
			var params  = {
						  Username					 : username,
						  Password        			 : password,
						  MailingGUID    			 : mailingGUID,
						  CustomerApplication        : customerApplication, 
						  Submit                     : submit,
						  PartialProcess             : partialProcess,
						  MaxPriceExVAT              : maxPriceExVAT, 
						  POReference                : pOReference,
						  PaymentMethod              : paymentMethod,
						  SkipPreviewImageGeneration : skipPreviewImageGeneration, 
						  EmailSuccessList           : emailSuccessList,
						  EmailErrorList             : emailErrorList, 
						  HttpPostOnSuccess          : httpPostOnSuccess,
						  HttpPostOnError 			 : httpPostOnError, 		 
						  ReturnFormat     			 : returnFormat
				};                          
			console.log(params);
			 methodName = 'ProcessMailing';
			 callbackMethod = callback;
			$.soap({
				url			: wsUrl,
				method		: methodName,
				SOAPAction	: '"'+ wsNamespace + methodName +'"', 
				data		: params,		
				success		: function (soapResponse) { responseFormatter(soapResponse)},
				error		: function (soapResponse) { responseFormatter(soapResponse)},		
				appendMethodToURL: false,        
				namespaceURL:	wsNamespace
			});
		};
		
};
