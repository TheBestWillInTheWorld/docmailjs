//requires
// jquery.min.js
// docmailAPIv2.js

$( document ).ready(function() {	
	$('#domain').append(location.protocol+'//'+document.domain);
});	

window.addEventListener('message', function(e) {
					var origin = e.origin;
					$('#message').empty();
					$('#message').append("Message Received: " + JSON.stringify(e.data));
					console.log($('#message'));	
					
					//handle async response
					var responder = function(result){
						console.log('calling back via parent.postMessage('+result+' ,'+origin+')');
						parent.postMessage(result,origin);
					};

					console.log(JSON.stringify(e.data));
					
					//wrapper for async GetBalance call
					var runGetBalance = function(data,callbackMethod){	
						var docmail = new DocmailJS();
						//TODO: validate fields and apply defaults
						docmail.getBalance(data.user,data.password,data.accountType,'json',responder);
					};
					var runCreateMailing = function(data,callbackMethod){
						//CreateMailing(username,password,customerApplication, productType,mailingName,mailingDescription,isMono, isDuplex,deliveryType,courierDeliveryToSelf, despatchASAP,despatchDate,addressNamePrefix, addressNameFormat,discountCode,minEnvelopeSize, returnFormat,callback)
						var docmail = new DocmailJS();
						docmail.createMailing(data.user,data.password,'DocmailJS','Postcard', 'DocmailJS from '+ origin, 'DocmailJS from '+ origin, false, true, 'Standard', false, true,'','', 'Full Name', '', '','json',responder);
					};
					var runAddDesignerTemplate = function(data,callbackMethod){
						//AddDesignerTemplate(sUsr, sPwd, gMailingGUID, sTemplateLayout, sDocType, "", bBleedSupplied, 0, True, sReturnMessageFormat)
						var docmail = new DocmailJS();
						docmail.addDesignerTemplate(data.user,data.password,data.mailingGUID,'Postcard A6 rear images', 'PostcardA6Right', '', true, 0, true,'json',responder);
					};
					var runAddDesignerImage = function(data,callbackMethod){
						//have to nest this due to async image loading
						var callAddDesignerImage = function(base64Image){
							//AddDesignerImage(sUsr, sPwd, gMailingGUID, "Background photo", sImgFileName, OpenFileAsByteArray(sImgFilePath), dImgRotation, "Crop", sReturnMessageFormat)
							var docmail = new DocmailJS();
							docmail.addDesignerImage(data.user,data.password,
							data.mailingGUID,data.partDisplayName,
							data.imageName,base64Image,
							data.imageRotation,data.imageFitOption,
							'json',responder);	
						};
						console.log("data.imageData="+ data.imageData);
						if (!data.imageData || typeof data.imageData == 'undefined' || data.imageData == 'undefined') {
							console.log('No image data supplied, fetching from imageUrl '+ data.imageUrl);
							//image loads are async, so we need to load the image and call back to callAddDesignerImage once we have the base64 image
							getBase64ImageFromURL(
										data.imageUrl,
										callAddDesignerImage
										);		
						}
						else{
							console.log('Image data supplied, using that');
							callAddDesignerImage(data.imageData);
						};
					};
					var runAddDesignerText = function(data,callbackMethod){
						//AddDesignerText(sUsr, sPwd, gMailingGUID, "Message", sTextMessage, 0, "", False, False, False, "", 0, 0, 0, sReturnMessageFormat)
						//username, password, mailingGUID, partDisplayName, textContent, fontSize, fontName, bold, italic, underline, textJustification, fontColourRed, fontColourGreen, fontColourBlue, returnFormat, callback
						var docmail = new DocmailJS();
						docmail.addDesignerText(data.user,data.password,data.mailingGUID, data.partDisplayName, data.textContent, data.fontSize, data.fontName, data.bold, data.italic, data.underline, data.textJustification,data.fontColourRed, data.fontColourGreen, data.fontColourBlue, 'json',responder);
					};
					//username,password,mailingGUID,address1,address2,address3,address4,address5,address6, useForProof,title,firstName,surname,fullname,jobTitle,companyName,email,telephone, directLine,mobile,facsimile,extraInfo,notes,customerAddressID,customerImportID, streamPages1,streamPages2,streamPages3,custom1,custom2,custom3,custom4,custom5, custom6,custom7,custom8,custom9,custom10,returnFormat,callback
					var runAddAddress = function(data,callbackMethod){
						//AddAddress(sUsr, sPwd, gMailingGUID, "Красноармейская ул., 278, Кв. 54", "Ростов-на-Дону, Ростовская область", "344022", "Russian Federation", "", "", True, "", "", "", "Тарасова Татьяна Васильевна", "", "", "", "", "", "", "", "", "", "", "", -1, -1, -1, "", "", "", "", "", "", "", "", "", "", sReturnMessageFormat)
						var addressLines = data.addressBlock.split('\n');
						
						var docmail = new DocmailJS();
						docmail.addAddress(data.user,data.password,data.mailingGUID, addressLines[0], addressLines[1], addressLines[2], addressLines[3], addressLines[4], addressLines[5],true,'','','',data.fullName,'','','','','','','','','','','','','','','','','','','','','','','','', 'json',responder);
					};
					var runProcessMailing = function(data,callbackMethod){
						//ProcessMailing(sUsr, sPwd, gMailingGUID, sAppName, True, False, 0, "", "Topup", True, sEmailOnError, sEmailOnSuccess, "", "", sReturnMessageFormat))
						//Username,Password,MailingGUID,CustomerApplication, Submit,PartialProcess,MaxPriceExVAT, POReference,PaymentMethod,SkipPreviewImageGeneration, EmailSuccessList,EmailErrorList, HttpPostOnSuccess,HttpPostOnError, ReturnFormat
						var docmail = new DocmailJS();
						docmail.processMailing(data.user,data.password,data.mailingGUID, 
						'DocmailJS', true, false, 999999, 'my ref', 'Topup', true,'will.gunby@gmail.com;will.gunby@cfh.com', 'will.gunby@gmail.com;will.gunby@cfh.com', '', '', 'json',responder);
					};
					
					var handler;
					var methodNotFound=false;
					//bind appropriate handlers
					if 		(e.data.method==='getBalance')			handler=runGetBalance
					else if (e.data.method==='createMailing') 		handler=runCreateMailing
					else if (e.data.method==='addDesignerTemplate') handler=runAddDesignerTemplate
					else if (e.data.method==='addDesignerImage') 	handler=runAddDesignerImage
					else if (e.data.method==='addDesignerText') 	handler=runAddDesignerText
					else if (e.data.method==='addAddress') 			handler=runAddAddress
					else if (e.data.method==='processMailing') 		handler=runProcessMailing
					else methodNotFound=true;
					
					if (methodNotFound==false) handler(e.data)
					else responder('Method ['+e.data.method+'] not found');
});

//TODO: create something to fill in blank images if not specified
/*
var addBlankImages = function(data){
		//have to nest this due to async image loading
		var callAddDesignerImage = function(base64Image){
			//AddDesignerImage(sUsr, sPwd, gMailingGUID, "Background photo", sImgFileName, OpenFileAsByteArray(sImgFilePath), dImgRotation, "Crop", sReturnMessageFormat)
			var docmail = new DocmailJS();
			docmail.addDesignerImage(data.user,data.password,
			data.mailingGUID,data.partDisplayName,
			data.imageName,base64Image,
			data.imageRotation,data.imageFitOption,
			'json',responder);	
		};

		//image loads are async, so we need to load the image and call back to callAddDesignerImage once we have the base64 image
		getBase64ImageFromURL(
					'./img/empty.png',
					callAddDesignerImage
					);		
};
			
*/